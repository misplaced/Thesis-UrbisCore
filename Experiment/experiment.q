[QueryGroup="Classes"] @collection [[
[QueryItem="Social-practice"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

select distinct ?x

where {
	?x a sp:Social-practice
}

[QueryItem="Agent"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

select distinct ?x
where {
	?x a sp:Rational-agent .
}

[QueryItem="Restaurant"]
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipantAndSocialRolesODP#>
PREFIX sp: <http://www.semanticweb.org/Social-practice-ODP#>



select distinct ?x 
where {
	?x a ex:Restaurant .
}

[QueryItem="Dining"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>


select ?x 
where {
	?x a ex:Dining .
}
]]

[QueryGroup="Properties"] @collection [[
[QueryItem="Used-for"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>


select ?x ?y 
where {
	?x ur:used-for ?y .
}

[QueryItem="Socially-used"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>


select ?x ?y
where {
	?x sp:socially-used-for ?y .
}
]]

[QueryGroup="SocialRoles"] @collection [[
[QueryItem="PlaceForItalianTourists"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

select ?placeForItalianTourists 
where {
	?placeForItalianTourists a ua:Urban-artefact .
	?placeForItalianTourists ur:socially-used-for ex:italianTouristDineOut .
}

[QueryItem="PlaceForForeignTourists"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

select ?placeForForeignTourists 
where {
	?placeForForeignTourists a ex:Restaurant .
	?placeForForeignTourists ur:socially-used-for ex:foreignTouristDineOut .
}

[QueryItem="PlaceForTurinLocals"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

select ?placeForTurinLocals 
where {
	?placeForTurinLocals ur:socially-used-for ex:turinLocalsDineOut .
}
]]

[QueryGroup="SpatialQuery"] @collection [[
[QueryItem="RestaurantNumber"]
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX ex: <http://www.semanticweb.org/experiment#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX xml: <http://www.w3.org/XML/1998/namespace>
PREFIX sp: <http://www.semanticweb.org/social-practice-ODP#>
PREFIX ur: <http://www.semanticweb.org/UrbanArtefactParticipationAndSocialRoles#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX ua: <http://www.semanticweb.org/UrbanArtefactODP#>

SELECT ?p ?c (COUNT(?p) as ?pc) {
  ?p a ex:Restaurant .
  ?p ua:has-location ?c .
}
]]
